# XNAT Dependency Getter #

This is the XNAT dependency getter. As its name suggests, it gets dependencies. It uses Gradle's dependency resolution and management features to
find artifacts such as war files, plugins, and so on, and download those artifacts to the appropriate folders in your deployment structure.

## Installing ##


